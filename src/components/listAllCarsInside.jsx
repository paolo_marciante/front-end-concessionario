import React, { Component } from 'react';
import AutovetturaService from '../services/AutovetturaService';

class listAllCarsInside extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cars: [],
    };
  }

  componentDidMount() {
    AutovetturaService.getAllCarsInside().then((res) => {
      this.setState({ cars: res.data });
    });
  }

  render() {
    return (
      <div className="container">
        <table className="table">
          <thead>
            <tr>
              <th scope="col">id</th>
              <th scope="col">alimentazione</th>
              <th scope="col">cilindrata</th>
              <th scope="col">tipoCambio</th>
              <th scope="col">colore</th>
              <th scope="col">annoProduzione</th>
              <th scope="col">configurazione</th>
              <th scope="col">marchio</th>
              <th scope="col">kw_T</th>
              <th scope="col">modello</th>
              <th scope="col">nTelaio</th>
            </tr>
          </thead>

          <tbody>
            {this.state.cars.map((car) => (
              <tr key={car.idConfigurazioneModello}>
                <td> {car.alimentazione} </td>
                <td> {car.cilindrata} </td>
                <td> {car.tipoCambio} </td>
                <td> {car.colore} </td>
                <td> {car.annoProduzione} </td>
                <td> {car.configurazione} </td>
                <td> {car.marchio} </td>
                <td> {car.kw_T} </td>
                <td> {car.modello} </td>
                <td> {car.ntelaio} </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    );
  }
}

export default listAllCarsInside;
