import React, { useState, useEffect } from 'react';
import apiCar from '../api/apiCar';
import { useParams } from 'react-router-dom';
import { anno, alimenta } from '../api/response';


export const Detail = () => {
  const params = useParams();
  const [err, setErr] = useState('');
  const [car, setCar] = useState({});

  const [alimentazione, setAlimentazione] = useState('');
  const [cilindrata, setCilindrata] = useState('');
  const [n_telai, setN_telai] = useState('');
  const [cambio, setCambio] = useState('');
  const [colore, setColore] = useState('');
  const [annoProduzione, setAnnoProduzione] = useState('');
  const [kw_T, setKw_T] = useState('');
  const [tipoCambio, setTipoCambio] = useState('');

  const [isEdit, setIsEdit] = useState(false);

  useEffect(() => {
    const fetchPost = async () => {
      try {
        const response = await apiCar.get(
          `/getAllCarInside/${params.id}`
        );
        setCar(response.data);
      } catch (e) {
        if (e.response) {
          console.log(e.response.data);
          console.log(e.response.status);
          console.log(e.response.headers);
        } else {
          setErr(`Errore: ${e.message}`);
          console.log(err);
        }
      }
    };
    fetchPost();
  }, [err, params]);

  const isModify = () => {
    setIsEdit(!isEdit);
  };

  const handleChange = (e) => {
    setAlimentazione(e.target.value);
  };

  console.log(isEdit);
  console.log(alimentazione);

  
  return (
    <div className="container">
      <div className="row">
        <div className="col-md-12">
          <h1 className="text-center mt-4">{car.marchio} {car.modello}</h1>
          <img
            className="rounded mx-auto d-block m-5 img-fluid"
            src="https://www.fiat.it/content/dam/fiat/cross/models/500-family-2020/opening/desktop/figurini/500/Hey-Google/500-hey-google-Model-page-gelato-white-glossy-black-Car-Desktop-680x430.png"
            alt=""
          />
          <div className="row">
            <div className="col-md-6 mx-auto"></div>
            <div className="row">
              <div
                className="btn-group"
                role="group"
                aria-label="Basic outlined example"
              >
                <button
                  onClick={() => isModify()}
                  type="button"
                  className="btn btn-outline-primary"
                >
                  {isEdit ? 'Salva' : 'Modifica'}
                </button>
              </div>

              <div className="col-md-6 mx-auto mt-5">
                <p>
                  Alimentazione:{car.alimentazione}
                  {isEdit ? (
                    <select onChange={''}>
                      <option selected>
                        Seleziona l'alimentazione
                      </option>
                      {alimenta.map((item) => (
                        <option>{item.al}</option>
                      ))}
                    </select>
                  ) : (
                    <b> {car.alimentazione}</b>
                  )}
                </p>
                <p>
                  Cilindrata: {car.cilindrata} 
                  {isEdit ? (
                    <input placeholder="cilindrata"></input>
                  ) : (
                    <b> </b>
                  )}
                </p>
                <p>
                  Tipo Cambio: {car.tipoCambio}
                </p>
                <p>
                  Colore: {car.colore}
                  {isEdit ? (
                    <input placeholder="colore"></input>
                  ) : (
                    <b> </b>
                  )}
                </p>
                <p>
                  Anno di produzione: {car.annoProduzione}
                  {isEdit ? (
                    <select>
                      <option selected>Seleziona l'anno</option>
                      {anno.map((item) => (
                        <option>{item.anno}</option>
                      ))}
                    </select>
                  ) : (
                    <b> </b>
                  )}
                </p>
                <p>
                  Kw_T: {car.kw_T}
                  {isEdit ? (
                    <input placeholder="kw_t"></input>
                  ) : (
                    <b> </b>
                  )}
                </p>
                <p>
                  nTelaio: {car.n_telai}
                  {isEdit ? (
                    <input placeholder="kw_t"></input>
                  ) : (
                    <b> </b>
                  )}
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
