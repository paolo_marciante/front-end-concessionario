import React, { useRef, useState } from "react";
import apiCar from "../../api/apiCar";

function PostVendita() {
  const prezzo = useRef(null);
  const nome = useRef(null);
  const cognome = useRef(null);
  const codiceFiscale = useRef(null);
  const nTelaio = useRef(null);
  const targa = useRef(null);
  const data = useRef(null);
  
  
  
  const [postResult, setPostResult] = useState(null);
   const fortmatResponse = (res) => {
    return JSON.stringify(res, null, 7);
  }; 
  async function postData() {
    const postData = {
      prezzo: prezzo.current.value,
      nome: nome.current.value,
      cognome: cognome.current.value,
      codiceFiscale: codiceFiscale.current.value,
      nTelaio: nTelaio.current.value,
      targa: targa.current.value,
      data : data.current.value,
    };
    try {
      const res = await apiCar.post("/addNewVenditaCliente/post" , postData, {
      
      });
      const result = {
        status: res.status + "-" + res.statusText,
        headers: res.headers,
        data: res.data,
      };
      setPostResult(fortmatResponse(result));
    } catch (err) {
      setPostResult(fortmatResponse(err.response?.data || err));
    }
  }
  const clearPostOutput = () => {
    setPostResult(null);
  };
  return (
    <form>
    <div className="container">
      <div className="row">
        <div className="col-md-12">
         
          <div className="row g-3">
            <div className="col-sm-4">
              <input
                type="text"
                className="form-control"
                ref={nome}
                placeholder="Nome"
              />
            </div>
            <div className="col-sm">
              <input
                type="text"
                className="form-control"
                ref={cognome}
                placeholder="cognome"
                aria-label="Zip"
              />
            </div>
          </div>
          <div className="row g-3 mt-1">
            <div className="col-sm-4">
              <input
                type="text"
                className="form-control"
                ref={codiceFiscale}
                placeholder="codice Fiscale"
              />
            </div>
            <div className="col-sm">
              <input
                type="text"
                className="form-control"
                ref={targa}
                placeholder="targa"
              />
            </div>
            <div className="col-sm">
              <input
                type="text"
                className="form-control"
                ref={nTelaio}
                placeholder="N° Telaio"
                aria-label="Zip"
              />
            </div>
            <div className="col-sm">
              <input
                type="text"
                className="form-control"
                ref={data}
                placeholder="Data"
                aria-label="Zip"
              />
            </div>
          </div>
        </div>
      </div>
      <div className="d-grid gap-2 mt-4">
      <button className="btn btn-sm btn-primary" onClick={postData}>Post Data</button>
          <button className="btn btn-sm btn-warning ml-2" onClick={clearPostOutput}>Pulisci Campi</button>
          { postResult && <div className="alert alert-secondary mt-2" role="alert"><pre>{postResult}</pre></div> }
      </div>
    </div>
  </form>
  );
}
export default PostVendita;


    
    
 