 import React, { useEffect, useState } from 'react';
import apiCar from '../../api/apiCar';
import { Card } from '../Card/Card';

export const FetchApi = () => {
  const [cars, setCars] = useState([]);
  const [errBe, setErrBe] = useState('');

  useEffect(() => {
    const fetchCars = async () => {
      try {
        const response = await apiCar.get('/getAllCarInside');
        setCars(response.data);
      } catch (e) {
        if (e.response) {
          console.log(e.response.data);
          console.log(e.response.status);
          console.log(e.response.headers);
        } else {
          setErrBe(`Errore: ${e.message} `);
          console.log(errBe);
        }
      }
    };
    fetchCars();
  }, [errBe]);

  console.log(cars);
  return (
    <>
      <h1>Ciaone</h1>

      {cars.map((car) => {
        return (
          <Card
            marchio={car.marchio}
            modello={car.modello}
            configurazione={car.configurazione}
          />
        );
      })}

      {/* {cars.map((car)=> <p>{car.marchio} {car.modello} {car.alimentazione}</p>)} */}
    </>
  );
};
 