import React, { useRef, useState } from "react";
import apiCar from "../../api/apiCar";

function PostAuto() {
  const id_configurazione_modello = useRef(null);
  const ntelaio = useRef(null);
  const url = useRef(null);
  
  
  
  const [postResult, setPostResult] = useState(null);
   const fortmatResponse = (res) => {
    return JSON.stringify(res, null, 2);
  }; 
  async function PostAuto() {
    const PostAuto = {
      id_configurazione_modello: id_configurazione_modello.current.value,
      ntelaio: ntelaio.current.value,
      url: url.current.value,
    };
    try {
      const res = await apiCar.post(`/autovettura/${id_configurazione_modello}/post` , PostAuto, {
        
        
        
      });
      const result = {
        status: res.status + "-" + res.statusText,
        headers: res.headers,
        data: res.data,
      };
      setPostResult(fortmatResponse(result));
     
    } catch (err) {
      setPostResult(fortmatResponse(err.response?.data || err));
    }
  }
  const clearPostOutput = () => {
    setPostResult(null);
  };
  return (
    <form>
    <div className="container">
      <div className="row mt-2">
        <div className="row-md-12">
            <div class="col-sm-2">
              <input
                type="text"
                class="form-control"
                ref={url}
                placeholder="Nome immagine.jpg"
              />
            </div>
            <div class="col-sm-2 mt-2">
              <input
                type="text"
                class="form-control"
                ref={id_configurazione_modello}
                placeholder="id della configurazione"
              />
            </div>
            <div class="col-sm-3 mt-2">
              <input
                type="text"
                class="form-control"
                ref={ntelaio}
                placeholder="N° Telaio"
                aria-label="Zip"
              />
            </div>
          </div>
        </div>
      </div>
      <div className="d-grid gap-2 mt-4">
      <button className="btn btn-sm btn-primary" onClick={PostAuto}>Post Data</button>
          <button className="btn btn-sm btn-warning ml-2" onClick={clearPostOutput}>Pulisci Campi</button>
          { postResult && <div className="alert alert-secondary mt-2" role="alert"><pre>{postResult}</pre></div> }
      </div>
  </form>
  );
}
export default PostAuto;
