import React, { useRef, useState } from "react";
import apiCar from "../../api/apiCar";

function PostConfigurazione() {
  const idModello = useRef(null);
  const annoProduzione = useRef(null);
  const alimentazione = useRef(null);
  const tipoCambio = useRef(null);
  const colore = useRef(null);
  const idConfigurazione = useRef(null);
  const cilindrata = useRef(null);
  const kw_T = useRef(null);
  
  const [postResult, setPostResult] = useState(null);
   const fortmatResponse = (res) => {
    return JSON.stringify(res, null, 8);
  }; 
  async function postData() {
    const postData = {
      idModello: idModello.current.value,
      idConfigurazione: idConfigurazione.current.value,
      annoProduzione: annoProduzione.current.value,
      alimentazione: alimentazione.current.value,
      tipoCambio: tipoCambio.current.value,
      colore: colore.current.value,
      cilindrata: cilindrata.current.value,
      kw_T: kw_T.current.value,
    };
    try {
      const res = await apiCar.post("/addConfigurazioneModello/post", postData, {

      });
      const result = {
        status: res.status + "-" + res.statusText,
        headers: res.headers,
        data: res.data,
      };
      setPostResult(fortmatResponse(result));
    } catch (err) {
      setPostResult(fortmatResponse(err.response?.data || err));
    }
  }
  const clearPostOutput = () => {
    setPostResult(null);
  };
  return (
    <form>
    <div className="container">
      <div className="row">
        <div className="col-md-12">
         
          <div className="row g-3">
            <div className="col-sm-4">
              <input
                type="text"
                className="form-control"
                ref={idModello}
                placeholder="Modello"
              />
            </div>
            <div className="col-sm">
              <input
                type="text"
                className="form-control"
                ref={annoProduzione}
                placeholder="Anno di produzione"
                aria-label="Zip"
              />
            </div>
          </div>
          <div className="row g-3 mt-1">
            <div className="col-sm-4">
              <input
                type="text"
                className="form-control"
                ref={alimentazione}
                placeholder="Alimentazione"
              />
            </div>
            <div className="col-sm">
              <input
                type="text"
                className="form-control"
                ref={tipoCambio}
                placeholder="Tipo di Cambio"
              />
            </div>
            <div className="col-sm">
              <input
                type="text"
                className="form-control"
                ref={colore}
                placeholder="Colore"
                aria-label="Zip"
              />
            </div>
          </div>
          <div className="row g-3 mt-1">
            <div className="col-sm-4">
              <input
                type="text"
                className="form-control"
                ref={idConfigurazione}
                placeholder="Configurazione"
              />
            </div>
            <div className="col-sm">
              <input
                type="text"
                className="form-control"
                ref={cilindrata}
                placeholder="Cilindrata"
              />
            </div>
            <div className="col-sm">
              <input
                type="text"
                className="form-control"
                ref={kw_T}
                placeholder="KwT"
                aria-label="Zip"
              />
            </div>
          </div>
        </div>
      </div>
      <div className="d-grid gap-2 mt-4">
      <button className="btn btn-sm btn-primary" onClick={postData}>Post Data</button>
          <button className="btn btn-sm btn-warning ml-2" onClick={clearPostOutput}>Pulisci Campi</button>
          { postResult && <div className="alert alert-secondary mt-2" role="alert"><pre>{postResult}</pre></div> }
      </div>
    </div>
  </form>
  );
}
export default PostConfigurazione;