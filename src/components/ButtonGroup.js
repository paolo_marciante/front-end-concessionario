import React from 'react';


export const ButtonGroup = ({ onClick, label }) => {
  return (
    <div className="btn-group" role="group" aria-label="Basic example">
      <button type="button" onClick={onClick} className="btn btn-primary">
        {label}
      </button>   
    </div>
  );
};
