import React, { useEffect, useState } from 'react';
import { Card } from './Card/Card';
import { Link } from 'react-router-dom';
import apiCar from '../api/apiCar';
import { ButtonGroup } from './ButtonGroup';
import PostAuto from './FetchApi/PostAuto';
import PostConfigurazione from './FetchApi/PostConfigurazione';

export const Home = () => {
  const [cars, setCars] = useState([]);
  const [errBe, setErrBe] = useState('');
  const [show, setShow] = useState(true);

  useEffect(() => {
    const fetchCars = async () => {
      try {
        const response = await apiCar.get('/getAllCarInside');
        setCars(response.data);
      } catch (e) {
        if (e.response) {
          console.log(e.response.data);
          console.log(e.response.status);
          console.log(e.response.headers);
        } else {
          setErrBe(`Errore: ${e.message} `);
          console.log(errBe);
        }
      }
    };
    fetchCars();
  }, [errBe]);

  const AggiungiAuto = () => {
    setShow(!show);
  };

  console.log(show);

  return (
    <>
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <ButtonGroup
              onClick={AggiungiAuto}
              label={
                show
                  ? 'Aggiungi Configurazione '
                  : 'Aggiungi Auto'
              }
            />
            {show ? (
              <PostAuto />
            ) : (
              <PostConfigurazione />
            )}

            <div className="row row-cols-1 row-cols-md-2 row-cols-xl-4 g-4 mt-5">
              {cars.map((car) => (
                <Card
                  marchioAuto={car.marchio}
                  modelloAuto={car.modello}
                  annoDiProduzione={car.annoProduzione}
                  colore={car.colore}
                  cilindrata={car.cilindrata}
                  dettaglio={
                    <Link
                      key={car.idConfigurazioneModello}
                      to={`dettaglio/${car.idConfigurazioneModello}`}
                    >
                      <button
                        type="button"
                        className="btn btn-primary"
                      >
                        Vai al dettaglio
                      </button>
                    </Link>
                  }
                  vendi={       
                    <a href="" class="btn btn-secondary" role="button">Vendi</a>                 
                }
                />
              ))}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
