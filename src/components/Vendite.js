import React, { useEffect, useState } from 'react';
import { CardVendita } from './Card/CardVendita';
import { Link } from 'react-router-dom';
import apiCar from '../api/apiCar';
import { ButtonGroup } from './ButtonGroup';
import { Detail } from './Detail';

export const Vendite = () => {
  const [vendite, setVendite] = useState([]);
  const [errBe, setErrBe] = useState('');
  const [show, setShow] = useState(true);
  

  


  useEffect(() => {
    const fetchVendite = async () => {
      try {
        const response = await apiCar.get('/getVenditeClienti');
        setVendite(response.data);
      } catch (e) {
        if (e.response) {
          console.log(e.response.data);
          console.log(e.response.status);
          console.log(e.response.headers);
        } else {
          setErrBe(`Errore: ${e.message} `);
          console.log(errBe);
        }
      }
    };
    fetchVendite();
  }, [errBe]);

  const aggiungiVendite = () => {
    setShow(!show);
  };

  console.log(show);

  return (
    <>
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <ButtonGroup
              onClick={aggiungiVendite}
              label={
                show
                  ? 'Annulla'
                  : 'Aggiungi vendite con cliente esistente'
              }
            />
            {show ? (
              <aggiungiVendite />
            ) : (
              <aggiungivenditeConClienteEsistente />
            )}

            <div className="row row-cols-1 row-cols-md-2 row-cols-xl-4 g-4 mt-5">
              {vendite.map((vendita) => (
                
                <CardVendita
                  data={vendita.data}
                  prezzo={vendita.prezzo}
                  nome={vendita.nome}
                  cognome={vendita.cognome}
                  codiceFiscale={vendita.codiceFiscale}
                  targa={vendita.targa}


                />
              ))}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
