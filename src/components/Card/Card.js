import React from 'react';

export const Card = ({
  marchioAuto,
  modelloAuto,
  annoDiProduzione,
  colore,
  cilindrata,
  dettaglio,
  vendi,
}) => {
  return (
    <div className="col">
      <div className="card" style={{ width: '18rem' }}>
        <img
          src="https://www.fiat.it/content/dam/fiat/cross/models/500-family-2020/opening/desktop/figurini/500/Hey-Google/500-hey-google-Model-page-gelato-white-glossy-black-Car-Desktop-680x430.png"
          className="card-img-top"
          alt="..."
        />
        <div className="card-body">
          <h5 className="card-title mb-4">
            {marchioAuto} - {modelloAuto}
          </h5>
          <p className="card-text">
            Anno di produzione: <b>{annoDiProduzione}</b>{' '}
          </p>
          <p className="card-text">
            {' '}
            Colore: <b>{colore}</b>{' '}
          </p>
          <p className="card-text">
            {' '}
            Cilindrata: <b>{cilindrata} CC</b>{' '}
          </p>
          {dettaglio}
          {vendi}
        </div>
      </div>
    </div>
  );
};
