import React from 'react';
import FetchImg from '../FetchApi/FetchImg'


export const CardVendita = ({
  data,
  prezzo,
  codiceFiscale,
  targa,
  dettaglio,
}) => {
  return (
    <div className="col">
      <div className="card text-dark bg-light mb-3" style={{ width: '18rem' }}>
        <img
          src={FetchImg}
          className="card-img-top"
          alt="..."
        />
        <div className="card-body">
          <h5 className="card-title text-center">
             DATA DI VENDITA
          </h5>
          <h6 className="card-title mb-4 fw-bold text-center"> 
           {data}
          </h6>

          <p className="card-text">
            Prezzo: <b>{prezzo} $</b>{' '}
          </p>
          <p className="card-text">
            {' '}
            Targa: <b>{targa}</b>{' '}
          </p>
          <p className="card-text">
            {' '}
            Codice Fiscale Cliente: <b>{codiceFiscale}</b>{' '}
          </p>
          {dettaglio}
        </div>
      </div>
    </div>
  );
};
