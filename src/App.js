import { Detail } from './components/Detail';
import { Home } from './components/Home';
import { Navbar } from './components/Navbar';
import { Vendite } from './components/Vendite';
import PostVendita from './components/FetchApi/PostVendita';
import {
  BrowserRouter as Router,
  Routes,
  Route,
} from 'react-router-dom';



export const App = () => {
  return (
    <>
      <Navbar />
      <Router>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="dettaglio/:id" element={<Detail />} />
          <Route path="vendi/" element={<PostVendita />} />
          <Route path="vendite/" element={<Vendite />} />
        </Routes>
      </Router>
    </>
  );
};
